#include <stdarg.h>
#define _DEFAULT_SOURCE
#include <unistd.h>
#include <stdio.h>
#include <stddef.h>
#include <stdlib.h>
#include <assert.h>

#include "mem_internals.h"
#include "mem.h"
#include "util.h"

void debug_block(struct block_header* b, const char* fmt, ... );
void debug(const char* fmt, ... );

extern inline block_size size_from_capacity( block_capacity cap );
extern inline block_capacity capacity_from_size( block_size sz );

static bool            block_is_big_enough( size_t query, struct block_header* block ) { return block->capacity.bytes >= query; }
static size_t          pages_count   ( size_t mem )                      { return mem / getpagesize() + ((mem % getpagesize()) > 0); }
static size_t          round_pages   ( size_t mem )                      { return getpagesize() * pages_count( mem ) ; }

//Инициализация блока по указанному addr, размером block_sz и указателем на следующий блок next
static void block_init( void* restrict addr, block_size block_sz, void* restrict next ) {
    *((struct block_header*)addr) = (struct block_header) {
        .next = next,
        .capacity = capacity_from_size(block_sz),
        .is_free = true
    };
}

static size_t region_actual_size( size_t query ) { return size_max( round_pages( query ), REGION_MIN_SIZE ); }

extern inline bool region_is_invalid( const struct region* r );



static void* map_pages(void const* addr, size_t length, int additional_flags) {
    return mmap( (void*) addr, length, PROT_READ | PROT_WRITE, MAP_PRIVATE | MAP_ANONYMOUS | additional_flags , 0, 0 );
}

/*  Аллоцировать регион памяти и инициализировать его блоком */
static struct region alloc_region  ( void const * addr, size_t query ) {
    //Для начала, узнаём размер блока памяти, который надо будет выделить
    const size_t actual_size = region_actual_size(query);
    //Для начала, попытаемся выделить память по желаемому адресу
    void* region_address = map_pages(addr, actual_size, MAP_FIXED_NOREPLACE);

    //Если выделить память по желаемому адресу не получилось,
    //т.к. она уже выделена для чего-то другого,
    //то в таком случае выделяем память где получится
    if (region_address == MAP_FAILED) {
        region_address = map_pages(addr, actual_size, 0);
        //Если же вновь не удалось выделить память, то возвращаем недействительный регион-пустышку
        if (region_address == MAP_FAILED) {
            return REGION_INVALID;
        }
    }

    //Далее, оформляем выделенный участок памяти в регион
    struct region allocated_region;
    allocated_region = (struct region) {
        .addr = region_address,
        .extends = false,
        .size = actual_size
    };

    //Инициализируем полученный регион блоком
    block_init(region_address, (block_size) {actual_size}, NULL);
    //Возвращаем полученный блок
    return allocated_region;

}

static void* block_after( struct block_header const* block );

//Инициализируем кучу и возвращаем её адрес инициализированным блоком
void* heap_init( size_t initial ) {
  const struct region region = alloc_region( HEAP_START, initial );
  if ( region_is_invalid(&region) ) return NULL;

  return region.addr;
}

#define BLOCK_MIN_CAPACITY 24

/*  --- Разделение блоков (если найденный свободный блок слишком большой )--- */

//Можем ли мы разделить блок на блок желаемой вместимости и остальное?
static bool block_splittable( struct block_header* restrict block, size_t query) {
  return block-> is_free && query + offsetof( struct block_header, contents ) + BLOCK_MIN_CAPACITY <= block->capacity.bytes;
}

static bool split_if_too_big( struct block_header* block, size_t query ) {
    if(block_splittable(block, query)){
        void* second_block_address = (void*)((uint8_t*)block + query + offsetof(struct block_header, contents));
        block_size second_block_size = {block->capacity.bytes - query};

        block_init(second_block_address, second_block_size, block->next);

        block->next = second_block_address;
        block->capacity.bytes = query;
        return true;
    }
    return false;
}


/*  --- Слияние соседних свободных блоков --- */

//Возвращает адрес, идущий за переданным блоком
static void* block_after( struct block_header const* block )              {
  return  (void*) (block->contents + block->capacity.bytes);
}

//Идёт ли второй блок snd сразу за первым блоком fst?
static bool blocks_continuous (struct block_header const* fst, struct block_header const* snd ) {
    return (void*)snd == block_after(fst);
}

//Можем ли мы объединить блоки fst и snd?
static bool mergeable(struct block_header const* restrict fst, struct block_header const* restrict snd) {
    return fst->is_free && snd->is_free && blocks_continuous( fst, snd ) ;
}

//Метод, пытающийся объединить блок с последующим блоком
static bool try_merge_with_next( struct block_header* block ) {

    if (block->next == NULL) {
        return false;
    }
    if (mergeable(block, block->next)){
        struct block_header* second_block = block->next;
        block->next = second_block->next;
        block->capacity.bytes += size_from_capacity(second_block->capacity).bytes;
        return true;
    }
    return false;
}


/*  --- ... ecли размера кучи хватает --- */

struct block_search_result {
  enum {
      BSR_FOUND_GOOD_BLOCK,
      BSR_REACHED_END_NOT_FOUND,
      BSR_CORRUPTED
  } type;
  struct block_header* block;
};

static struct block_search_result find_good_or_last  ( struct block_header* restrict block, size_t sz )    {

    //Проверяем, не слишком ли маленький блок мы хотим ли найти
    size_t actual_size= sz > BLOCK_MIN_CAPACITY? sz : BLOCK_MIN_CAPACITY;

    struct block_header* cur_block = block;
    struct block_header* last_block = block;

    while(cur_block != NULL){
        //Если текущий блок свободен...
        if (cur_block->is_free) {
            //сперва пытаемся объединить блок со всеми последующими свободными блоками, если таковые имеются
            while(try_merge_with_next(cur_block));
            //затем проверяем, достаточно ли большой получился блок
            if (block_is_big_enough(actual_size, cur_block)){
                //если блок достаточно большой, то возвращаем информацию о нём
                return (struct block_search_result) {.type = BSR_FOUND_GOOD_BLOCK, .block = cur_block};
            }
        }
        //Запоминаем этот блок на случай, если он последний
        last_block = cur_block;
        //Переходим к следующему блоку
        cur_block = cur_block->next;
    }

    //Если подходящий блок не найден, возвращаем об этом информацию, передав последний блок
    return (struct block_search_result) {
        .type = BSR_REACHED_END_NOT_FOUND,
        .block = last_block
    };
}

/*  Попробовать выделить память в куче начиная с блока `block` не пытаясь расширить кучу
 Можно переиспользовать как только кучу расширили. */
static struct block_search_result try_memalloc_existing ( size_t query, struct block_header* block ) {
    struct block_search_result bsr = find_good_or_last(block, query);
    //Если был найден подходящий блок
    if (bsr.type == BSR_FOUND_GOOD_BLOCK) {
        //Если найденный блок достаточно велик, разделим его на две части
        split_if_too_big(bsr.block, query);
    }
    return bsr;
}



static struct block_header* grow_heap( struct block_header* restrict last, size_t query ) {
    const block_size wanted_size = size_from_capacity((block_capacity){query});
    //Узнаём адрес ячейки память, стоящей сразу за блоком last
    void const*  wanted_address = (void*)((uint8_t*)last + size_from_capacity(last->capacity).bytes);
    //Аллоцируем регион, стараясь разместить его по желаемому адресу
    struct region allocated_region = alloc_region(wanted_address, wanted_size.bytes);
    //Инициализируем новый регион блоком
    block_init(allocated_region.addr, (block_size){allocated_region.size}, NULL);

    struct block_header* const new_block = (struct block_header*) allocated_region.addr;

    last->next = new_block;

    //Попробуем объединить последний блок старого региона и новый блок
    if(try_merge_with_next(last)) {
        return last;
    } else {
        return new_block;
    }
}

/*  Реализует основную логику malloc и возвращает заголовок выделенного блока */
static struct block_header* memalloc( size_t query, struct block_header* heap_start) {

    size_t size_to_allocate = size_max(query, BLOCK_MIN_CAPACITY);
    struct block_search_result bsr = try_memalloc_existing(size_to_allocate, heap_start);

    //Если найден подходящий блок, переходим ниже

    //Если нет подходящего блока, наращиваем наш регион
    if (bsr.type == BSR_REACHED_END_NOT_FOUND){
        bsr.block = grow_heap(bsr.block, size_to_allocate);
        //Если блок слишком большой, разделим его
        split_if_too_big(bsr.block, size_to_allocate);
    }
    //Если возникла ошибка, попробуем выделить регион
    if (bsr.type == BSR_CORRUPTED) {
        bsr.block = heap_init(size_to_allocate);
        //Если блок слишком большой, разделим его
        split_if_too_big(bsr.block, size_to_allocate);
    }

    //Пометим, что этот блок теперь не свободен
    bsr.block->is_free = false;
    return bsr.block;
}

void* _malloc( size_t query ) {
  struct block_header* const addr = memalloc( query, (struct block_header*) HEAP_START);
  if (addr) return addr->contents;
  else return NULL;
}

struct block_header* block_get_header(void* contents) {
  return (struct block_header*) (((uint8_t*)contents)-offsetof(struct block_header, contents));
}

void _free( void* mem ) {
  if (!mem) return ;
  struct block_header* header = block_get_header(mem);
  header->is_free = true;
  //При освобождении блока, следует объединить его со всеми последующими свободными блоками
  while(try_merge_with_next(header));
}
